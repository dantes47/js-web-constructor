import image from '../src/assets/image.png';
import {TextBlock, TitleBlock, ImageBlock, TextColumnsBlock} from './classes/blocks'; 
import {css} from './utils';

// console.log(image);

export const model = [
    new TitleBlock('Web Sites Constructor in Pure JavaScript!', {
        tag: 'h2',
        styles: css({
            background: 'background: linear-gradient(to right, #ff0099, #493240)',
            color: '#ff0099',
            padding: '1.5rem',
            'text-align': 'center'
        }),
        // styles: 'background: linear-gradient(to right, #ff0099, #493240); color: #ff0099; padding: 1.5rem; text-align: center;'
    }),

    new ImageBlock(image, {
        styles: 'padding: 2rem 0; display: flex; justifay-content: center',
        alt: 'my_image',
        imageStyles: 'width: 500px; height: auto'
    }),

    new TextBlock('As you may see - JavaScript is really "a Live" & expressive language!', {
        styles: 'background: linear-gradient(to left, #f2994a, #f2c94c); font-weight: bold; color: brown; text-align: center; padding: 1rem;'
    }),

    new TextColumnsBlock([
       'WebApps in Pure JS, without any outside-libraries', 
       'Principles of SOLID & OOP in JS with one shot', 
       'JS => it\'s NOT that simple, but it\'s FUN!'
    ], {
        styles: 'background: linear-gradient(to left, #f2994a, #f2c94c); font-weight: bold; padding: 1rem;' 
    })
];